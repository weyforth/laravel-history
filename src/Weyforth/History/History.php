<?php
/**
 * History Laravel extension.
 *
 * Keeps track of page views in session and allows searching
 * and indexing of pages to avoid using troublesome 'Back'
 * function provided by Laravel's Redirector
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\History;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

class History
{

    /**
     * Maximum number of pages to store in history.
     *
     * @var integer
     */
    protected $maxPages = 10;

    /**
     * History session key.
     *
     * @var integer
     */
    protected $sessionKey = 'history';

    /**
     * Stores the last found history state.
     *
     * @var integer
     */
    protected $last = null;


    /**
     * Retrieve all of the stored history data from the session.
     *
     * @return array
     */
    protected function getSessionStore()
    {
        return Session::get($this->sessionKey, array());
    }


    /**
     * Adds the current route page to the session store.
     *
     * @param string $uri    URI of the page to store.
     * @param string $route  Route name of the page to store.
     * @param array  $params Array of params for the route.
     *
     * @return void
     */
    protected function addToSessionStore($uri, $route, array $params)
    {
        $store = $this->getSessionStore();

        if (count($store) > 0
            && $store[(count($store) - 1)]->uri == $uri
        ) {
            return false;
        }

        $store[] = (object) array(
            'uri' => $uri,
            'route' => $route,
            'params' => $params
        );

        while (count($store) > $this->maxPages) {
            array_shift($store);
        }

        Session::set($this->sessionKey, $store);
    }


    /**
     * Adds the current route page to the history.
     *
     * @return void
     */
    public function add()
    {
        $route = Route::getCurrentRoute();
        if ($route) {
            $this->addToSessionStore(
                Request::getUri(),
                $route->getName(),
                $route->parameters()
            );
        }
    }


    /**
     * Retrieve all of the stored history pages.
     *
     * @return array[object]
     */
    public function all()
    {
        return $this->getSessionStore();
    }


    /**
     * Clear the history.
     *
     * @return void
     */
    public function destroy()
    {
        Session::remove($this->sessionKey);
    }


    /**
     * Find the last page with the specified route name.
     *
     * @param string $name Name of the route to find.
     *
     * @return Weyforth\History\History For chainability
     */
    public function last($name)
    {
        if (!is_array($name)) {
            $name = array($name);
        }

        $store = $this->getSessionStore();
        for ($i = (count($store) - 1); $i >= 0; $i--) {
            if (in_array($store[$i]->route, $name)) {
                $this->last = $i;
                break;
            }
        }

        return $this;
    }


    /**
     * Find the last page in the history.
     *
     * @return Weyforth\History\History For chainability
     */
    public function end()
    {
        $store = $this->getSessionStore();
        if (count($store) > 0) {
            $this->last = (count($store) - 1);
        } else {
            return false;
        }

        return $this;
    }


    /**
     * Increments the stored page and return it.
     *
     * @param integer $i The amount to increment by.
     *
     * @return object or false
     */
    public function plus($i)
    {
        $store = $this->getSessionStore();
        if ($this->last !== null
            && array_key_exists(
                $final = ($this->last + $i),
                $store
            )
        ) {
            $return        = $store[$final];
            $return->index = $final;

            return $return;
        }

        return false;
    }


    /**
     * Decrements the stored page and return it.
     *
     * @param integer $i The amount to decrement by.
     *
     * @return object or false
     */
    public function minus($i)
    {
        $index = ($i * -1);

        return $this->plus($index);
    }


    /**
     * Return the stored page if it exists.
     *
     * @return object or false
     */
    public function get()
    {
        $store = $this->getSessionStore();
        if ($this->last !== null) {
            $return = array_key_exists($this->last, $store) ?
                $store[$this->last] :
                false;
            if ($return) {
                $return->index = $this->last;
            }

            return $return;
        }

        return false;
    }


    /**
     * Redirect response to the last page with the specified route name.
     *
     * If a page with the specified route name is not found,
     * redirect to the fallback route, which must be specified
     * and we can't guarantee a page will be found. Will also
     * apply the supplied parameters to the fallback route.
     *
     * @param string $last           Name of the route to find.
     * @param string $fallback       Name of the fallback route.
     * @param array  $fallbackParams Indexed array of params to apply to the fallback route.
     *
     * @return Illuminate\Http\RedirectResponse
     */
    public function redirectLast($last, $fallback, array $fallbackParams = array())
    {
        if ($found = $this->last($last)->get()) {
            return Redirect::route($found->route, $found->params);
        }

        return Redirect::route($fallback, $fallbackParams);
    }


}
